resource "aws_s3_bucket" "bvb_recipe_app_public_files" {
  bucket        = "${local.prefix}-bvb-files"
  acl           = "public-read"
  force_destroy = true
}
